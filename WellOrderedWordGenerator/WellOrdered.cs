﻿using System;
using System.Collections.Generic;

namespace WellOrderedWordGenerator
{
	public static class WellOrdered
	{
		public static int AllPossibleCombinations(int i)
		{
			var stringArray = new List<string>();
			OrderedStrings("", i, stringArray);

			Console.WriteLine(string.Join(Environment.NewLine, stringArray));
			Console.WriteLine("count is: {0}", stringArray.Count);
			return stringArray.Count;
		}


		private static void OrderedStrings(string currentString, int length, List<string> stringArray)
		{
			if (length == 0)
			{
				stringArray.Add(currentString);
			}
			else if (length > 52)
			{ // should be less than the number of all letters doubled (uppercase/lowercase)
				Console.WriteLine("error: position can't be bigger than 52");
			}
			else
			{
				int nextChar = 'a';
				if (currentString.Length > 0)
				{
					int lastChar = currentString[currentString.Length - 1];
					if (char.IsLower((char)lastChar))
					{
						nextChar = lastChar + 1;
					}
					else if (char.IsUpper((char)lastChar))
					{
						nextChar = char.ToLower((char)lastChar) + 1;
					}
				}
				for (int i = nextChar; i <= 'z'; i++)
				{
					char toInsert = (char)i;
					if (char.IsLetter(toInsert))
					{ //is letter
						OrderedStrings(currentString + toInsert, length - 1, stringArray); //add the lower case char
						OrderedStrings(currentString + char.ToUpper(toInsert), length - 1, stringArray); //add the upper case char
					}
				}
			}
		}
	}
}
