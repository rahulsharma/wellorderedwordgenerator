﻿using System;

namespace WellOrderedWordGenerator
{
	class Program
	{
		static void Main(string[] args)
		{
			int combinationNumber = args.Length > 0 ? Convert.ToInt32(args[0]) : 3;

			WellOrdered.AllPossibleCombinations(combinationNumber);
			Console.ReadLine();
		}
	}
}
