﻿using System;
using NUnit.Framework;
using WellOrderedWordGenerator;

namespace Test
{
	[TestFixture]
	public class UnitTest1
	{
		[Test]
		public void TotalNumberofWordsShouldbeMatchForNumberofLetters()
		{
			int two = 1300;
			int three = 20800;
			int four = 239200;
			int five = 2104960;


			Assert.AreEqual(two, WellOrdered.AllPossibleCombinations(2));
			Assert.AreEqual(three, WellOrdered.AllPossibleCombinations(3));
			Assert.AreEqual(four, WellOrdered.AllPossibleCombinations(4));
			Assert.AreEqual(five, WellOrdered.AllPossibleCombinations(5));
		}
	}
}
